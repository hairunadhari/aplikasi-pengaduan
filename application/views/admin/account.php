<!-- Content wrapper -->
<div class="content-wrapper">
	<!-- Content -->
	<div class="container-xxl flex-grow-1 container-p-y">

		<!-- Basic Bootstrap Table -->
		<div class="card">
			<div class=" d-flex justify-content-between">
				<h5 class="card-header">User Management</h5>
				
			</div>
			<a type="button" class="btn btn-success ms-3 mb-3" style="width:150px;" href="<?= base_url('Administrator/form_add_account')?>">Tambah User</a>
			
			<?= $this->session->flashdata('message'); ?>
			<div class="table-responsive text-nowrap">
				<table class="table">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Username</th>
							<th>No Telepon</th>
							<th>Role</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody class="table-border-bottom-0">
						<?php $no = 1?>
						<?php foreach ($akun as $row) : ?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= $row['name'] ?></td>
							<td><?= $row['username'] ?></td>
							<td><?= $row['telp'] ?></td>
							<?php if($row['role_id'] == 1){?>
							<td><?= 'Administrator'?></td>
							<?php } elseif($row['role_id'] == 2){?>
							<td><?= 'Petugas'?></td>
							<?php }?>
							<td>
								<a class="" href="<?= base_url('Administrator/edit_account/'.$row['petugas_id'])?>"><i class="bx bx-edit"></i></a> /
								<a style="color: red;" onclick="return confirm('Apakah anda yakin ingin menghapus user ini?')" href="<?= base_url('Administrator/delete_account/'.$row['petugas_id'])?>"><i class="bx bx-trash"></i></a>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- / Content -->
