<!DOCTYPE html>
<html><head>
        <title>Laporan_pdf</title>
    <style>
         table{
                width: 100%;
                border-collapse: collapse;
                color: black;
                border-bottom:1px solid black;
                margin-top: 20px;

            },
            table th{
                border: 1px solid black;
                padding: 7px 17px;
            },
            table tr td{
                text-align: center;
            },
            table td{
               border-right: 1px solid black;
               border-left: 1px solid black;
               height: 70px;
            },
            h2{
                text-align:center;
            }
    </style>
    <h2>Laporan Pengaduan Masyarakat</h2>
   <table>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Tanggal Pengaduan</th>
        <th>Isi Laporan</th>
        <th>Status</th>
    </tr>
    <?php $no = 1?>
    <?php foreach ($pengaduan as $p):?>
        <tr>
            <td><?=$no++?></td>
            <td><?=$p['name']?></td>
            <td><?=$p['tgl_pengaduan']?></td>
            <td><?=$p['isi_laporan']?></td>
            <td><?=$p['status']?></td>
        </tr>
    <?php endforeach?>
    
   </table>
</body></html>
