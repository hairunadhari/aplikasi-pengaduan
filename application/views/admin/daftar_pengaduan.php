<!-- Content wrapper -->
<div class="content-wrapper">
	<!-- Content -->
	<div class="container-xxl flex-grow-1 container-p-y">

		<!-- Basic Bootstrap Table -->
		<div class="card">
			<h4 class="fw-bold p-3 mb-2"><span class="text-muted fw-light"></span>Data Semua Pengaduan</h4>
			<div class="nav-item d-flex justify-content-between align-items-center m-4">
				<form class="d-flex" action="" method="post">
					<input class="form-control me-2" type="text" name="search" placeholder="Search..."
						aria-label="Search">
				</form>
				<?php if ($this->session->userdata('role_id') == 1) { ?>
					<div class="buttonsx">
						<a href="<?= base_url('Administrator/pdf')?>" class="btn btn-primary"><i class="bx bx-printer"></i>
						Export PDF</a>
						<a href="<?= base_url('Administrator/excel')?>" class="btn btn-primary"><i
						class="bx bx-printer"></i> Export EXCEL</a>
					</div>
				<?php }?>
			</div>
			<?= $this->session->flashdata('message'); ?>
			<div class="table-responsive p-3">
				<table class="table" id="tabledaftarpengaduan">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Tanggal Pengaduan</th>
							<th>Isi Laporan</th>
							<th>Foto</th>
							<th>Status</th>
							<th>Opsi</th>
						</tr>
					</thead>
					<tbody class="table-border-bottom-0">
						<?php $no = 1?>
						<?php foreach ($pengaduan as $p) {?>
						<tr>
							<td><?=$no++?></td>
							<td><?=$p['name']?></td>
							<td><?=$p['tgl_pengaduan']?></td>
							<td>
							<button type="button" class="btn btn-primary" data-bs-toggle="modal"
								data-bs-target="#laporan<?= $p['id']?>">lihat laporan
							</button>
							</td>
							<td><img src="<?php echo base_url() . 'uploads/' . $p['foto'] ?>" width="150"
									style="margin: auto;"></td>
							<td>
								<?php if ($p['status'] == 'Diproses') { ?>
									<span class="badge bg-primary">Diproses</span>
								<?php } else {?>
										<span class="badge bg-success">Selesai</span>
								<?php } ?>
							</td>
							<td>
								<div class="dropdown">
									<button type="button" class="btn p-0 dropdown-toggle hide-arrow"
										data-bs-toggle="dropdown">
										<i class="bx bx-dots-vertical-rounded"></i>
									</button>
									<div class="dropdown-menu">
									<?php if ($p['status'] == 'Diproses') { ?>
										<div class="dropdown-item">
											<button type="button" class="btn btn-primary" data-bs-toggle="modal"
											data-bs-target="#basicModal<?= $p['id']?>">
											Beri Tanggapan
											</button>
										</div>
									<?php } elseif ($p['status'] == 'Selesai') { ?>
										<div class="dropdown-item">
											<button type="button" class="btn btn-success" data-bs-toggle="modal"
											data-bs-target="#tanggapan<?= $p['id']?>">
											Lihat Tanggapan
											</button>
										</div>
									<?php } ?>
										<a class="dropdown-item" href="<?= base_url('Administrator/edit_pengaduan/'.$p['id'])?>"><i
												class="bx bx-edit-alt me-1"></i> Edit</a>
										<a class="dropdown-item" href="<?= base_url('Administrator/delete_pengaduan/'.$p['id'])?>" onclick="return confirm('Apakah anda yakin ingin mengahapus pengaduan ini?');">
										<i class="bx bx-trash me-1"></i> Delete</a>
									</div>
								</div>
							</td>
						</tr>
						<?php }?>
					</tbody>
				</table>
				<!-- MODAL -->
				<?php foreach ($pengaduan as $p) : ?>
				<div class="modal fade" id="basicModal<?= $p['id']?>" tabindex="-1" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel1">Form Tanggapan</h5>
								<button type="button" class="btn-close" data-bs-dismiss="modal"
									aria-label="Close"></button>
							</div>
							<form action="<?= base_url('Administrator/input_tanggapan')?>" method="post"
								enctype="multipart/form-data">
								<div class="modal-body">
									<div class="row">
										<div class="col mb-3">
											<label for="nameBasic" class="form-label">Isi Tanggapan</label>
											<input type="hidden" name="id_pengaduan" value="<?= $p['id']?>" />
											<input type="hidden" name="id_petugas"
												value="<?= $this->session->userdata('role_id')?>" />
											<input type="text" name="tanggapan" class="form-control"
												placeholder="Tanggapan Anda" />
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
										Close
									</button>
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</form>

						</div>
					</div>
				</div>
				<?php endforeach; ?>

				<!-- TUTUP MODAL -->
				<?php foreach ($pengaduan as $p) : ?>
				<div class="modal fade" id="laporan<?= $p['id']?>" tabindex="-1" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel1">Isi Laporan</h5>
								<button type="button" class="btn-close" data-bs-dismiss="modal"
									aria-label="Close"></button>
							</div>
							<form action="<?= base_url('Administrator/input_tanggapan')?>" method="post"
								enctype="multipart/form-data">
								<div class="modal-body">
								<?= $p['isi_laporan']?>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
										Close
									</button>
								</div>
							</form>

						</div>
					</div>
				</div>
				<?php endforeach; ?>

				<!-- TUTUP MODAL -->
				<!-- TUTUP MODAL -->
				<?php foreach ($pengaduan as $p) : ?>
				<div class="modal fade" id="tanggapan<?= $p['id']?>" tabindex="-1" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel1">Isi Laporan</h5>
								<button type="button" class="btn-close" data-bs-dismiss="modal"
									aria-label="Close"></button>
							</div>
							<form action="<?= base_url('Administrator/input_tanggapan')?>" method="post"
								enctype="multipart/form-data">
								<div class="modal-body">
								<?= $p['tanggapan']?>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
										Close
									</button>
								</div>
							</form>

						</div>
					</div>
				</div>
				<?php endforeach; ?>

				<!-- TUTUP MODAL -->
			</div>
		</div>
	</div>
</div>
<!-- / Content -->
