<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>Lapor</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
   
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets-admin/assets/img/favicon/favicon.ico')?>">
    <link rel="manifest" href="<?= base_url('assets/img/favicons/manifest.json')?>">
    <meta name="msapplication-TileImage" content="assets/img/favicons/mstile-150x150.png">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="<?= base_url('assets/css/theme.css')?>" rel="stylesheet" />
   

</head>