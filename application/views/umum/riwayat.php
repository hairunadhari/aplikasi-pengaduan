<section class="py-0 " style="margin-bottom: 100px;">
	<div class="bg-holder"
		style="background-image:url(assets/img/illustrations/dot.png);background-position:left;background-size:auto;margin-top:-105px;">
	</div>
	<div class="bg-holder"
		style="background-image:url(assets/img/illustrations/dot.png);background-size:auto;margin-top: -5px;">
	</div>
	<div class="bg-holder"
		style="background-image:url(assets/img/illustrations/dot.png);background-position:right;background-size:auto;margin-top: 150px; margin-left: -100px;">
	</div>
	<!--/.bg-holder-->

	<div class="container position-relative" style="margin-top: 100px;">
		<div class="row align-items-center">

			<div class="card mt-5" style="box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;">
				<div class="card">
					<h5 class="card-header mt-2">Riwayat Pengaduan Saya</h5>
					<div class="table-responsive text-nowrap">
						<table class="table">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Tanggal Pengaduan</th>
									<th>Isi Laporan</th>
									<th>Status</th>
									<th>Tanggapan</th>
								</tr>
							</thead>
							<tbody class="table-border-bottom-0">
								<?php $no = 1?>
								<?php foreach ($masyarakat as $m ) { ?>
								<tr>
									<td><?= $no++?></td>
									<td><?= $m['name']?></td>
									<td><?= date('d M Y', strtotime($m['tgl_pengaduan'])) ?></td>
									<td>
									<button type="button" class="btn btn-primary" data-bs-toggle="modal"
											data-bs-target="#laporan<?= $m['id']?>">Lihat Laporan </button>
									</td>
									<td>
										<?php if ($m['status'] == 'Diproses') {?>
										<span class="badge bg-primary me-1">Diproses</span>
										<?php } else{?>
										<span class="badge bg-success me-1">Selesai</span>
										<?php } ?>
									</td>
									<td>
										<?php if ($m['status'] == 'Diproses') {?>
										<span>Belum ada tanggapan</span>
										<?php } else{?>
										<button type="button" class="btn btn-primary" data-bs-toggle="modal"
											data-bs-target="#exampleModal<?= $m['id']?>"> Lihat Tanggapan </button>
										<?php } ?>
									</td>
								</tr>
								<?php }?>
							</tbody>
						</table>
						<!-- Modal -->
						<?php foreach ($masyarakat as $m) : ?>
						<div class="modal fade" id="exampleModal<?= $m['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel"
							aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Isi Tanggapan</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close"></button>
									</div>
									<div class="modal-body"><?= $m['tanggapan']?></div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach ?>

						<?php foreach ($masyarakat as $m) : ?>
						<div class="modal fade" id="laporan<?= $m['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel"
							aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Laporan Anda</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close"></button>
									</div>
									<div class="modal-body"><?= $m['isi_laporan']?></div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
