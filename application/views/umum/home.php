<section class="py-0 " style="margin-bottom: 100px;">
	<div class="bg-holder"
		style="background-image:url(assets/img/illustrations/dot.png);background-position:left;background-size:auto;margin-top:-105px;">
	</div>
	<div class="bg-holder"
		style="background-image:url(assets/img/illustrations/dot.png);background-size:auto;margin-top: -5px;">
	</div>
	<div class="bg-holder"
		style="background-image:url(assets/img/illustrations/dot.png);background-position:right;background-size:auto;margin-top: 150px; margin-left: -100px;">
	</div>
	<!--/.bg-holder-->
	<?php if($this->session->userdata('masyarakat_id')){?>
	<div class="container position-relative" style="margin-top: 100px;">
		<div class="row align-items-center">

			<div class="card m-auto w-75" style="box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;">
				<div class="test">
					<div class="card-header m-2 d-flex justify-content-between align-items-center">
						<h5 class="mb-0">Sampaikan Laporan Anda!</h5>
					</div>
					<div class="card-body">
						<form>
							<div hidden class="mb-3">
								<label class="form-label" for="basic-default-fullname">Id</label>
								<input type="text" class="form-control" value="<?= $masyarakat['masyarakat_id']; ?>"
									id="basic-default-fullname" placeholder="John Doe">
							</div>
							<div class="mb-3">
								<label class="form-label" for="basic-default-fullname">Nik</label>
								<input type="text" class="form-control" value="<?= $masyarakat['nik']; ?>"
									id="basic-default-fullname" placeholder="John Doe">
							</div>
							<div class="mb-3">
								<label class="form-label" for="basic-default-company">Nama</label>
								<input type="text" class="form-control" value="<?= $masyarakat['name']; ?>"
									id="basic-default-company" placeholder="ACME Inc.">
							</div>
							<div class="mb-3">
								<label class="form-label" for="basic-default-phone">No Handphone</label>
								<input type="text" id="basic-default-phone" value="<?= $masyarakat['telp']; ?>"
									class="form-control phone-mask" placeholder="658 799 8941">
							</div>
							<div class="mb-3">
								<label class="form-label" for="basic-default-message">Isi Laporan</label>
								<textarea id="basic-default-message" rows="5" class="form-control"
									placeholder="Isi laporan..."></textarea>
							</div>
							<div class="mb-3">
								<label class="form-label" for="basic-default-company">Foto</label>
								<input type="file" class="form-control" id="basic-default-company">
							</div>
							<button type="submit" class="btn btn-primary">Send</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } else {?>
	<div class="container position-relative">
		<div class="row align-items-center">
			<div class="col-md-5 col-lg-6 order-md-1 pt-8 ">
				<img class="img-fluid" style="margin-left: 50px;" src="assets2/images/hero.png" alt="" />
			</div>
			<div class="col-md-7 col-lg-6 text-center text-md-start pt-5 pt-md-8">
				<h1 class="mb-4 display-3 fs-5 fw-bold">Aplikasi Pelaporan dan Pengaduan Masyarakat</h1>
				<p class=" mb-4 fs-1">Lapor online adalah layanan penyampaian semua aspirasi dan pengaduan masyarakat
					indonesia melalui kanal berbasis website. Ayo tulis laporanmu jika kamu sedang dalam kesulitan, kami
					akan selalu siap membantumu!</p>
				<a class="btn btn-lg btn-outline-primary rounded-pill z-index-2 hover-top" href="<?= base_url('Auth')?>"
					role="button">Daftar
					/ Login</a>

			</div>
		</div>
	</div>
	<?php } ?>

</section>
