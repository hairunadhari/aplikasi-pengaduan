<?php

class auth_model extends CI_Model {

    public function get_petugas_by_username($username) {
        return $this->db->get_where('petugas', ['username' => $username])->row_array();
    }
     
    public function get_masyarakat_by_username($username) {
        return $this->db->get_where('masyarakat', ['username' => $username])->row_array();
    }

    public function getall_data_masyarakat_by_id($id) {
        return $this->db->get_where('masyarakat', array('masyarakat_id' => $id))->row_array();
      
    }
    public function getall_pengaduan_by_id($id)
    {
        $this->db->select('masyarakat.masyarakat_id, masyarakat.name, pengaduan.id, pengaduan.tgl_pengaduan, pengaduan.isi_laporan, pengaduan.foto, pengaduan.status, tanggapan.tanggapan');
        $this->db->from('masyarakat');  
        $this->db->join('pengaduan','masyarakat.masyarakat_id = pengaduan.id_masyarakat','right');  
        $this->db->join('tanggapan','pengaduan.id = tanggapan.id_pengaduan','left');  
        $this->db->where('pengaduan.id_masyarakat', $id);  
        return $this->db->get()->result_array();
    }
     
}
